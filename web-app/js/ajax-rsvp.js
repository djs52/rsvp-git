// closure generating functions for ajax service functions
function beforeSendGen(idstring) {
    // show the spinning refresh icon and the "saving..." text
    return function(jqXHR, settings) {
        $("#result_" + idstring)
            .removeClass()
            .addClass('icon-refresh icon-spin');
        $("#comment_" + idstring).text("saving...");
    }
}

function successGen(idstring) {
    // show the tick icon and text returned by the ajax call
    return function(data,textStatus){
        if (console) console.log(data);
        $("#result_" + idstring)
            .removeClass()
            .addClass('icon-ok');
        $("#comment_" + idstring).text(data);
        $("#result_" + idstring).parents(".control-group").removeClass("error");
    }
}

function errorGen(idstring) {
    // show the warning icon and text returned by the ajax call or error
    return function(data,textStatus,errorThrown){
        if (console) console.log(data);
        if (console) console.log(errorThrown);
        $("#result_" + idstring)
            .removeClass()
            .addClass('icon-warning-sign');
        if (data.responseText) {
            $("#comment_" + idstring).text(data.responseText);
        } else {
            $("#comment_" + idstring).text(data.statusText);
        }
        $("#result_" + idstring).parents(".control-group").addClass("error");
    }
}

// AJAX state save for +1 names
$( ".guestname" ).change(function() {
    var s = this.id.split("_");
    var id = s[1];

    $.ajax({type:'POST', 
            data:{'id':id, 'name':this.value},
            url:'/rsvp/guest/ajaxGuest',
            beforeSend:beforeSendGen("name_" + id),
            success:successGen("name_" + id),
            error:errorGen("name_" + id)
           });
});
// AJAX state save for contact fields
$( ".emergency" ).change(function() {
    var id = this.id;
    
    $.ajax({type:'POST', 
            data:{'fieldName':id, 'value':this.value},
            url:'/rsvp/party/ajaxEmergency',
            beforeSend:beforeSendGen(id),
            success:successGen(id),
            error:errorGen(id)
           });
});

// Initialise accordions
$( ".collapse" ).collapse({toggle:false});

// AJAX state save for RSVP buttons
$( ".rsvp .btn-group button" ).click(function() {
    var s = this.id.split("_");
    var response = s[0];
    var id = s[1];

    // expand/collapse the accordion
    btngroup = $(this).parents(".btn-group")
    if (response == "rsvpyes") {
        $(btngroup.attr('accordion')).collapse('show')
    } else {
        $(btngroup.attr('accordion')).collapse('hide')
    };

    $.ajax({type:'POST', 
            data:{'response':response},
            url:'/rsvp/guest/ajaxResponse/' + id,
            beforeSend:beforeSendGen("rsvp_" + id),
            success:successGen("rsvp_" + id),
            error:errorGen("rsvp_" + id)
           });
});

// AJAX state save for menu selections
$( ".menu .btn-group button" ).click(function() {
    var s = this.id.split("_");
    var item = s[1];
    var choice = s[2];
    var id = s[3];

    $.ajax({type:'POST', 
            data:{'item':item,'value':choice},
            url:'/rsvp/guest/ajaxMenu/' + id,
            beforeSend:beforeSendGen(item + "_" + id),
            success:successGen(item + "_" + id),
            error:errorGen(item + "_" + id)
           });
});
