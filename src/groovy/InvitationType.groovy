package rsvp;

public enum InvitationType implements org.springframework.context.MessageSourceResolvable {
    FULL,
    CEREMONY,
    EVENING,
    ADMIN

    // resolve names via i18n
    public Object[] getArguments() { [] as Object[] }
    public String[] getCodes() { [ name() ] }
    public String getDefaultMessage() { "?-" + name() }
}