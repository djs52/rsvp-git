package rsvp;

public enum Course implements org.springframework.context.MessageSourceResolvable {
    STARTER,
    MAIN,
    PUDDING
    
    // resolve names via i18n
    public Object[] getArguments() { [] as Object[] }
    public String[] getCodes() { [ name() ] }
    public String getDefaultMessage() { "?-" + name() }
}