package rsvp;

public enum RSVPStatus implements org.springframework.context.MessageSourceResolvable {
    COMING,
    NOT_COMING,
    NO_RESPONSE

    // resolve names via i18n
    public Object[] getArguments() { [] as Object[] }
    public String[] getCodes() { [ name() ] }
    public String getDefaultMessage() { "?-" + name() }
}