import rsvp.*

fixture {
    admin(Party, name:"Admin", RSVPcode:123456789, type:InvitationType.ADMIN)

    m1(MealChoice, course:Course.STARTER, name:"Salmon")
    m2(MealChoice, course:Course.STARTER, name:"Melon")
    m3(MealChoice, course:Course.MAIN, name:"Vegetables")
    m4(MealChoice, course:Course.MAIN, name:"Duck")
    m5(MealChoice, course:Course.PUDDING, name:"Tart")
    m6(MealChoice, course:Course.PUDDING, name:"Cheese")

    smith(Party, name:"Smith")
    smith1(Guest, party:smith, name:"Joe Smith")
    smith2(Guest, party:smith, name:"Fred Smith")

    bloggs(Party, name:"Bloggs")
    bloggs1(Guest, party:bloggs, name:"Jane Bloggs")
    bloggs2(Guest, party:bloggs, name:"Fred Bloggs")

    windsor(Party, name:"Windsor/James", type:InvitationType.CEREMONY)
    windsor1(Guest, party:windsor, name:"Adam Windsor")
    windsor2(Guest, party:windsor, name:"Sarah James")

    harlow(Party, name:"Harlow", type:InvitationType.CEREMONY)
    harlow1(Guest, party:harlow, name:"Charles Harlow")
    harlow2(Guest, party:harlow)

}