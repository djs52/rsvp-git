package rsvp



import org.junit.*
import grails.test.mixin.*

@TestFor(MealChoiceController)
@Mock(MealChoice)
class MealChoiceControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/mealChoice/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.mealChoiceInstanceList.size() == 0
        assert model.mealChoiceInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.mealChoiceInstance != null
    }

    void testSave() {
        controller.save()

        assert model.mealChoiceInstance != null
        assert view == '/mealChoice/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/mealChoice/show/1'
        assert controller.flash.message != null
        assert MealChoice.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/mealChoice/list'

        populateValidParams(params)
        def mealChoice = new MealChoice(params)

        assert mealChoice.save() != null

        params.id = mealChoice.id

        def model = controller.show()

        assert model.mealChoiceInstance == mealChoice
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/mealChoice/list'

        populateValidParams(params)
        def mealChoice = new MealChoice(params)

        assert mealChoice.save() != null

        params.id = mealChoice.id

        def model = controller.edit()

        assert model.mealChoiceInstance == mealChoice
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/mealChoice/list'

        response.reset()

        populateValidParams(params)
        def mealChoice = new MealChoice(params)

        assert mealChoice.save() != null

        // test invalid parameters in update
        params.id = mealChoice.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/mealChoice/edit"
        assert model.mealChoiceInstance != null

        mealChoice.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/mealChoice/show/$mealChoice.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        mealChoice.clearErrors()

        populateValidParams(params)
        params.id = mealChoice.id
        params.version = -1
        controller.update()

        assert view == "/mealChoice/edit"
        assert model.mealChoiceInstance != null
        assert model.mealChoiceInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/mealChoice/list'

        response.reset()

        populateValidParams(params)
        def mealChoice = new MealChoice(params)

        assert mealChoice.save() != null
        assert MealChoice.count() == 1

        params.id = mealChoice.id

        controller.delete()

        assert MealChoice.count() == 0
        assert MealChoice.get(mealChoice.id) == null
        assert response.redirectedUrl == '/mealChoice/list'
    }
}
