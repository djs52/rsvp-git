package rsvp



import org.junit.*
import grails.test.mixin.*

@TestFor(GuestController)
@Mock(Guest)
class GuestControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/guest/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.guestInstanceList.size() == 0
        assert model.guestInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.guestInstance != null
    }

    void testSave() {
        controller.save()

        assert model.guestInstance != null
        assert view == '/guest/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/guest/show/1'
        assert controller.flash.message != null
        assert Guest.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/guest/list'

        populateValidParams(params)
        def guest = new Guest(params)

        assert guest.save() != null

        params.id = guest.id

        def model = controller.show()

        assert model.guestInstance == guest
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/guest/list'

        populateValidParams(params)
        def guest = new Guest(params)

        assert guest.save() != null

        params.id = guest.id

        def model = controller.edit()

        assert model.guestInstance == guest
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/guest/list'

        response.reset()

        populateValidParams(params)
        def guest = new Guest(params)

        assert guest.save() != null

        // test invalid parameters in update
        params.id = guest.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/guest/edit"
        assert model.guestInstance != null

        guest.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/guest/show/$guest.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        guest.clearErrors()

        populateValidParams(params)
        params.id = guest.id
        params.version = -1
        controller.update()

        assert view == "/guest/edit"
        assert model.guestInstance != null
        assert model.guestInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/guest/list'

        response.reset()

        populateValidParams(params)
        def guest = new Guest(params)

        assert guest.save() != null
        assert Guest.count() == 1

        params.id = guest.id

        controller.delete()

        assert Guest.count() == 0
        assert Guest.get(guest.id) == null
        assert response.redirectedUrl == '/guest/list'
    }
}
