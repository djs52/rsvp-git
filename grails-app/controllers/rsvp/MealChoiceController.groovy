package rsvp

import org.springframework.dao.DataIntegrityViolationException

class MealChoiceController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    def index() {
        redirect action: 'menu', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [mealChoiceInstanceList: MealChoice.list(params), mealChoiceInstanceTotal: MealChoice.count()]
    }

    def menu() {
        def partyInstance = Party.get(session.user);
        if (partyInstance?.type != InvitationType.FULL && partyInstance?.type != InvitationType.ADMIN) {
            flash.message = "Access to the menu not allowed for this user.";
            redirect(controller: 'party', action:'rsvp');
        }
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[mealChoiceInstance: new MealChoice(params)]
			break
		case 'POST':
	        def mealChoiceInstance = new MealChoice(params)
	        if (!mealChoiceInstance.save(flush: true)) {
	            render view: 'create', model: [mealChoiceInstance: mealChoiceInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), mealChoiceInstance.id])
	        redirect action: 'show', id: mealChoiceInstance.id
			break
		}
    }

    def show() {
        def mealChoiceInstance = MealChoice.get(params.id)
        if (!mealChoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), params.id])
            redirect action: 'list'
            return
        }

        [mealChoiceInstance: mealChoiceInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def mealChoiceInstance = MealChoice.get(params.id)
	        if (!mealChoiceInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [mealChoiceInstance: mealChoiceInstance]
			break
		case 'POST':
	        def mealChoiceInstance = MealChoice.get(params.id)
	        if (!mealChoiceInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (mealChoiceInstance.version > version) {
	                mealChoiceInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'mealChoice.label', default: 'MealChoice')] as Object[],
	                          "Another user has updated this MealChoice while you were editing")
	                render view: 'edit', model: [mealChoiceInstance: mealChoiceInstance]
	                return
	            }
	        }

	        mealChoiceInstance.properties = params

	        if (!mealChoiceInstance.save(flush: true)) {
	            render view: 'edit', model: [mealChoiceInstance: mealChoiceInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), mealChoiceInstance.id])
	        redirect action: 'show', id: mealChoiceInstance.id
			break
		}
    }

    def delete() {
        def mealChoiceInstance = MealChoice.get(params.id)
        if (!mealChoiceInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), params.id])
            redirect action: 'list'
            return
        }

        try {
            mealChoiceInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'mealChoice.label', default: 'MealChoice'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
