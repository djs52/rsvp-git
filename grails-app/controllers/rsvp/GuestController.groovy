package rsvp

import org.springframework.dao.DataIntegrityViolationException

class GuestController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

    /** Populate section for each guest */
    def options(int id) {
        def guestInstance = Guest.get(id);
        if (!guestInstance) {
            flash.message = "Guest not found!"
        } else {
            if (session.user != guestInstance.party.id) {
                flash.message = "Access to this guest is not authorised. Please log in again.";
                redirect('/');
            } else {
                return [guestInstance: guestInstance];
            }
        }
    }

    /** Ajax update function: +1 name */
    def ajaxGuest(int id, String name) {
        def guestInstance = Guest.get(id);
        if (!guestInstance) {
            render(status: 401, text: "failed, please log in again")
        } else if (guestInstance.party.id != session.user) {
            render(status: 401, text: "failed, please log in again")
        } else {
            guestInstance.name = name
            if (!guestInstance.save(flush: true)) {
                render(status: 503, text: "failed")
            } else {
                render "saved"
            }
        }
    }

    /** Ajax update function: dietary requirements */
    def ajaxDiet(int id, String dietaryRequirements) {
        def guestInstance = Guest.get(id);
        if (!guestInstance) {
            render(status: 401, text: "failed, please log in again")
        } else if (guestInstance.party.id != session.user) {
            render(status: 401, text: "failed, please log in again")
        } else {
            guestInstance.dietaryRequirements = dietaryRequirements
            if (!guestInstance.save(flush: true)) {
                render(status: 503, text: "failed")
            } else {
                render "saved"
            }
        }
    }

    /** Ajax update function: response */
    def ajaxResponse(int id, String response) {
        def guestInstance = Guest.get(id);
        if (!guestInstance) {
            render(status: 401, text: "failed, please log in again")
        } else if (guestInstance.party.id != session.user) {
            render(status: 401, text: "failed, please log in again")
        } else {
            if (response == "rsvpyes") {
                guestInstance.response = RSVPStatus.COMING;
            } else if (response == "rsvpno") {
                guestInstance.response = RSVPStatus.NOT_COMING;
            } else {
                guestInstance.response = RSVPStatus.NO_RESPONSE;
            }
            if (!guestInstance.save(flush: true)) {
                render(status: 503, text: "failed")
            } else {
                render "saved"
            }
        }
    }

    /** Ajax update function: menu choice */
    def ajaxMenu(int id, String item, int value) {
        def guestInstance = Guest.get(id);
        def mealChoiceInstance = MealChoice.get(value);

        if (!guestInstance) {
            render(status: 401, text: "failed, please log in again")
        } else if (guestInstance.party.id != session.user) {
            render(status: 401, text: "failed, please log in again")
        } else if (value != 0 && !mealChoiceInstance) {
            render(status: 401, text: "failed, please log in again")
        } else {
            if (item == "starter") {
                guestInstance.starter = mealChoiceInstance;
            } else if (item == "main") {
                guestInstance.main = mealChoiceInstance;
            } else if (item == "pudding") {
                guestInstance.pudding = mealChoiceInstance;
            } else {
                render(status: 503, text: "failed")
            }
            if (!guestInstance.save(flush: true)) {
                render(status: 503, text: "failed")
            } else {
                render "saved"
            }
        }
    }


    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        def guestList;
        def count;

        params.max = Math.min(params.max ? params.int('max') : 10, 100)

        switch (params.filter) {
        case "COMING": 
        guestList = Guest.findAllByResponse(RSVPStatus.COMING, params); 
        count = Guest.countByResponse(RSVPStatus.COMING);
        break;

        case "NOT_COMING": 
        guestList = Guest.findAllByResponse(RSVPStatus.NOT_COMING, params); 
        count = Guest.countByResponse(RSVPStatus.NOT_COMING);
        break;

        case "NO_RESPONSE": 
        guestList = Guest.findAllByResponse(RSVPStatus.NO_RESPONSE, params); 
        count = Guest.countByResponse(RSVPStatus.NO_RESPONSE);
        break;

        default: 
        guestList = Guest.list(params);
        count = Guest.count();
        }

        [guestInstanceList: guestList, guestInstanceTotal: count, filter:params.filter]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[guestInstance: new Guest(params)]
			break
		case 'POST':
	        def guestInstance = new Guest(params)
	        if (!guestInstance.save(flush: true)) {
	            render view: 'create', model: [guestInstance: guestInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'guest.label', default: 'Guest'), guestInstance.id])
	        redirect action: 'show', id: guestInstance.id
			break
		}
    }

    def show() {
        def guestInstance = Guest.get(params.id)
        if (!guestInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'guest.label', default: 'Guest'), params.id])
            redirect action: 'list'
            return
        }

        [guestInstance: guestInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def guestInstance = Guest.get(params.id)
	        if (!guestInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'guest.label', default: 'Guest'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [guestInstance: guestInstance]
			break
		case 'POST':
	        def guestInstance = Guest.get(params.id)
	        if (!guestInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'guest.label', default: 'Guest'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (guestInstance.version > version) {
	                guestInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'guest.label', default: 'Guest')] as Object[],
	                          "Another user has updated this Guest while you were editing")
	                render view: 'edit', model: [guestInstance: guestInstance]
	                return
	            }
	        }

	        guestInstance.properties = params

	        if (!guestInstance.save(flush: true)) {
	            render view: 'edit', model: [guestInstance: guestInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'guest.label', default: 'Guest'), guestInstance.id])
	        redirect action: 'show', id: guestInstance.id
			break
		}
    }

    def delete() {
        def guestInstance = Guest.get(params.id)
        if (!guestInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'guest.label', default: 'Guest'), params.id])
            redirect action: 'list'
            return
        }

        try {
            guestInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'guest.label', default: 'Guest'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'guest.label', default: 'Guest'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
