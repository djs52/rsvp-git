package rsvp

import org.springframework.dao.DataIntegrityViolationException

class PartyController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST'];

    /** Allow users to log in */
    def login(String RSVPcode) {
        // normalise the code
        def partyInstance;
        try {
            def code = RSVPcode.replaceAll('[ ,-]','').toInteger();
            partyInstance = Party.findWhere(RSVPcode:code);
        } catch (NumberFormatException e) {
            partyInstance = null;
        }
        if (!partyInstance) {
            flash.message = 'Sorry, your RSVP code was not recognised';
            redirect(url:'/');
        } else {
            println "login: ${partyInstance.name}"
            session.user = partyInstance.id;
            if (partyInstance.type == InvitationType.ADMIN) {
                redirect controller: 'guest', action: 'list';
            } else {
                redirect action:'rsvp';
            }
        }
    }

    /** The main user response page */
    def rsvp() {
        def partyInstance = Party.get(session.user);
        [partyInstance: partyInstance];
    }

    /** Ajax update function: emergency contact details */
    def ajaxEmergency(String fieldName, String value) {
        def partyInstance = Party.get(session.user);
        if (!partyInstance) {
            render(status: 401, text: "failed, please log in again")
        } else {
            if (fieldName == "phone") {
                partyInstance.emergencyPhone = value;
            } else if (fieldName == "email") {
                partyInstance.emergencyEmail = value;
            } else {
                render(status: 401, text: "failed, please log in again")
            }

            if (!partyInstance.save(flush: true)) {
                render(status: 503, text: "failed")
            } else {
                render "saved"
            }
        }
    }

    /** Ajax update function: message */
    def ajaxMessage(int id, String message) {
        def partyInstance = Party.get(id);
        if (!partyInstance) {
            render(status: 401, text: "failed, please log in again")
        } else if (partyInstance.id != session.user) {
            render(status: 401, text: "failed, please log in again")
        } else {
            partyInstance.notes = message;
            if (!partyInstance.save(flush: true)) {
                render(status: 503, text: "failed")
            } else {
                render "saved"
            }
        }
    }

    def index() {
        redirect action: 'rsvp', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [partyInstanceList: Party.list(params), partyInstanceTotal: Party.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[partyInstance: new Party(params)]
			break
		case 'POST':
	        def partyInstance = new Party(params)
	        if (!partyInstance.save(flush: true)) {
	            render view: 'create', model: [partyInstance: partyInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'party.label', default: 'Party'), partyInstance.id])
	        redirect action: 'show', id: partyInstance.id
			break
		}
    }

    def show() {
        def partyInstance = Party.get(params.id)
        if (!partyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'party.label', default: 'Party'), params.id])
            redirect action: 'list'
            return
        }

        [partyInstance: partyInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def partyInstance = Party.get(params.id)
	        if (!partyInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'party.label', default: 'Party'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [partyInstance: partyInstance]
			break
		case 'POST':
	        def partyInstance = Party.get(params.id)
	        if (!partyInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'party.label', default: 'Party'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (partyInstance.version > version) {
	                partyInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'party.label', default: 'Party')] as Object[],
	                          "Another user has updated this Party while you were editing")
	                render view: 'edit', model: [partyInstance: partyInstance]
	                return
	            }
	        }

	        partyInstance.properties = params

	        if (!partyInstance.save(flush: true)) {
	            render view: 'edit', model: [partyInstance: partyInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'party.label', default: 'Party'), partyInstance.id])
	        redirect action: 'show', id: partyInstance.id
			break
		}
    }

    def delete() {
        def partyInstance = Party.get(params.id)
        if (!partyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'party.label', default: 'Party'), params.id])
            redirect action: 'list'
            return
        }

        try {
            partyInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'party.label', default: 'Party'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'party.label', default: 'Party'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
