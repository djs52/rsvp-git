<%@ page import="rsvp.Party" %>
<%@ page import="rsvp.InvitationType" %>
<!doctype html>
<html>
    <head>
	<meta name="layout" content="bootstrap"/>
	<g:set var="entityName" value="${message(code: 'party.label', default: 'Party')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<r:require modules="rsvp"/>
    </head>
        <body data-spy="scroll" data-target="#sidenav" data-offset="40">
	    <div class="row-fluid">
		
		<div class="span3" id="sidenav">
		    <div class="well" data-spy="affix" data-offset-top="0">
			<ul class="nav nav-pills nav-stacked">
                            <g:each var="g" in="${partyInstance.guests.sort{it.id}}">
			        <li>
                                    <a href="#id${g.id}">
                                        <i class="icon-user icon-white"></i>
                                        ${(g.name?.split(' ')[0] ?: 'Guest').encodeAsHTML()}
                                    </a>
			        </li>
                            </g:each>
			    <li>
                                <a href="#emergency">
                                    <i class="icon-warning-sign icon-white"></i>
                                    Contact info
                                </a>
			    </li>
			    <li>
                                <a href="#message">
                                    <i class="icon-edit icon-white"></i>
                                    Message
                                </a>
			    </li>
			</ul>
		    </div>
		</div>

		<div class="span9">
		    


		    <div class="page-header">
			<h1>RSVP for your party</h1>
		    </div>

                    <p>
                        You can RSVP
                        <g:if test="${partyInstance.type == InvitationType.FULL}">
                            and make your menu choices 
                        </g:if>
                        here on the website. Please make sure that we
                        have your reply by 31 July.
                    </p>
                    <g:if test="${partyInstance.type == InvitationType.FULL}">
                        <p>
                            You can see the full list of menu options on the <g:link controller="mealChoice" action="menu">menu</g:link> page.
                        </p>
                    </g:if>


		    <g:if test="${flash.message}">
			<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
		    </g:if>
		    

                    <g:each var="g" in="${partyInstance.guests.sort{it.id}}">
                        <g:include controller="guest" action="options" id="${g.id}"/>
                    </g:each>

                    <section id="emergency">
                        <form class="form-horizontal">
                            <legend>Contact information</legend>
                            <p>How should we get in touch if something goes wrong?</p>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-prepend">
                                        <span class="add-on"><i class="icon-phone"></i></span>
                                        <input class="emergency" id="phone" type="text" placeholder="Phone number" value="${partyInstance.emergencyPhone}"/>
                                    </div>
                                    <i id="result_phone"></i><span id="comment_phone"></span>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <div class="input-prepend">
                                        <span class="add-on"><i class="icon-envelope"></i></span>
                                        <input class="emergency" id="email" type="text" placeholder="Email address" value="${partyInstance.emergencyEmail}"/>
                                    </div>
                                    <i id="result_email"></i><span id="comment_email"></span>
                                </div>
                             </div>
                         </form>
                    </section>

                    <section id="message">
                        <form class="form-horizontal">
                            <legend>Leave us a message</legend>
                            <p>Anything else to say?</p>

                            <div class="control-group">
                                <div class="controls">
                                    <a href="#message_${partyInstance.id}" role="button" class="btn btn-block btn-group-vertical btn-food" data-toggle="modal">Say it here</a>
                                    <i id="result_message_${partyInstance.id}"></i> <span id="comment_message_${partyInstance.id}"></span>
                                </div>
                    
                            </div>
                        </form>
                    </section>

		</div>

	    </div>


    <%-- Modal --%>
    <div id="message_${partyInstance.id}" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="messageLabel_${partyInstance.id}" aria-hidden="true">
        <%-- grails is a bit too helpful -- we can't use the
        generating functions in ajax-rsvp.js in advance, so call
        them and then call their results --%>
	<g:formRemote name="message_${partyInstance.id}" 
                      url="[controller:'party', action:'ajaxMessage', params:[id:partyInstance?.id]]" 
                      class="form-horizontal" 
                      action="edit" 
                      id="${partyInstance?.id}" 
                      after="\$('#message_${partyInstance.id}').modal('hide')"
                      onSuccess="successGen('message_'+${partyInstance.id})(data,textStatus)"
                      onFailure="errorGen('message_'+${partyInstance.id})(XMLHttpRequest,textStatus,errorThrown)">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="messageLabel_${partyInstance.id}">Message to Dan and Rachel<br/>
                <small>from the ${partyInstance.name.encodeAsHTML()} party</small></h3>
            </div>
            <div class="modal-body">
	        <g:textArea name="message" class="modal-input" rows="5" maxlength="255" value="${partyInstance?.notes}"/>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary">Save changes</button>
            </div>
        </g:formRemote>
    </div>






	</body>




    </html>
