
<%@ page import="rsvp.Party" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'party.label', default: 'Party')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>
				
					<g:if test="${partyInstance?.name}">
						<dt><g:message code="party.name.label" default="Name" /></dt>
						
							<dd><g:fieldValue bean="${partyInstance}" field="name"/></dd>
						
					</g:if>
				
					<g:if test="${partyInstance?.displayName}">
						<dt><g:message code="party.displayName.label" default="DisplayName" /></dt>
						
							<dd><g:fieldValue bean="${partyInstance}" field="displayName"/></dd>
						
					</g:if>
				
					<g:if test="${partyInstance?.emergencyPhone}">
						<dt><g:message code="party.emergencyPhone.label" default="Emergency Phone" /></dt>
						
							<dd><g:fieldValue bean="${partyInstance}" field="emergencyPhone"/></dd>
						
					</g:if>
				
					<g:if test="${partyInstance?.emergencyEmail}">
						<dt><g:message code="party.emergencyEmail.label" default="Emergency Email" /></dt>
						
							<dd><g:fieldValue bean="${partyInstance}" field="emergencyEmail"/></dd>
						
					</g:if>
				
					<g:if test="${partyInstance?.RSVPcode}">
						<dt><g:message code="party.RSVPcode.label" default="RSVP code" /></dt>
						
							<dd><g:fieldValue bean="${partyInstance}" field="RSVPcode"/></dd>
						
					</g:if>
				
					<g:if test="${partyInstance?.type}">
						<dt><g:message code="party.type.label" default="Type" /></dt>
						
							<dd><g:fieldValue bean="${partyInstance}" field="type"/></dd>
						
					</g:if>
				
					<g:if test="${partyInstance?.notes}">
						<dt><g:message code="party.notes.label" default="Notes" /></dt>
						
							<dd><g:fieldValue bean="${partyInstance}" field="notes"/></dd>
						
					</g:if>

					<g:if test="${partyInstance?.guests}">
						<dt><g:message code="party.guests.label" default="Guests" /></dt>
						
							<g:each in="${partyInstance.guests}" var="g">
							<dd><g:link controller="guest" action="show" id="${g.id}">${g?.encodeAsHTML()}</g:link></dd>
							</g:each>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${partyInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn" action="edit" id="${partyInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
