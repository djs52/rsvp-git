<%@ page import="rsvp.Party" %>



<div class="fieldcontain ${hasErrors(bean: partyInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="party.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="100" value="${partyInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: partyInstance, field: 'displayName', 'error')} ">
	<label for="displayName">
		<g:message code="party.displayName.label" default="DisplayName" />
		
	</label>
	<g:textField name="displayName" maxlength="100" value="${partyInstance?.displayName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: partyInstance, field: 'emergencyPhone', 'error')} ">
	<label for="emergencyPhone">
		<g:message code="party.emergencyPhone.label" default="Emergency Phone" />
		
	</label>
	<g:textField name="emergencyPhone" maxlength="30" value="${partyInstance?.emergencyPhone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: partyInstance, field: 'emergencyEmail', 'error')} ">
	<label for="emergencyEmail">
		<g:message code="party.emergencyEmail.label" default="Emergency Email" />
		
	</label>
	<g:field type="email" name="emergencyEmail" maxlength="100" value="${partyInstance?.emergencyEmail}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: partyInstance, field: 'RSVPcode', 'error')} required">
	<label for="RSVPcode">
		<g:message code="party.RSVPcode.label" default="RSVP code" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="number" name="RSVPcode" min="100000000" max="999999999" value="${fieldValue(bean: partyInstance, field: 'RSVPcode')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: partyInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="party.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="type" from="${rsvp.InvitationType?.values()}" keys="${rsvp.InvitationType.values()*.name()}" required="" value="${partyInstance?.type?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: partyInstance, field: 'guests', 'error')} ">
	<label for="guests">
		<g:message code="party.guests.label" default="Guests" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${partyInstance?.guests?}" var="g">
    <li><g:link controller="guest" action="show" id="${g.id}">${g?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="guest" action="create" params="['party.id': partyInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'guest.label', default: 'Guest')])}</g:link>
</li>
</ul>

</div>

