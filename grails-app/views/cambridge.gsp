<!doctype html>
<html>
    <head>
	<meta name="layout" content="bootstrap"/>
	<title>Cambridge</title>
    </head>
    
    <body data-spy="scroll" data-target="#sidenav" data-offset="50">
        <!-- <header class="jumbotron subhead" id="overview"> -->
        <!--     <div class="container"> -->
        <!--         <h1>Wedding information</h1> -->
        <!--         <p class="lead">Places to stay, eat, park; things to buy, things to wear</p> -->
        <!--     </div> -->
        <!-- </header> -->
        
	    <div class="row-fluid">
		<div class="span3" id="sidenav">
		    <div class="well" data-spy="affix" data-offset-top="0">

                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#dining">Dining</a></li>
                            <li><a href="#staying">Staying</a></li>
                            <li><a href="#relaxing">Relaxing</a></li>
                        </ul>
                    </div>
                </div>

                <div class="span9">

                <section class="first-no-title">
                <h2>Dining in Cambridge</h2>
                <p>Unfortunately we are not able to invite all of our guests to dinner.  If you are in need of somewhere else to eat, there are a wide range of places in Cambridge.  We&rsquo;ve listed some of our favourites here, but there are plenty more options as well.</p>
	<p>There are two branches of Pizza Express, one on <a href="http://www.pizzaexpress.com/visit-a-restaurant/restaurant/cambridge-jesus-lane/">Jesus Lane</a>, and the other on <a href="http://www.pizzaexpress.com/visit-a-restaurant/restaurant/cambridge-regent-street/">Regent Street</a>.  For Asian food, we like Japanese restaurant <a href="http://www.teri-aki.co.uk/">Teri-Aki</a> and Kerelan (South Indian) restaurant <a href="http://www.riceboat.com/">The Rice Boat</a>.</p>
	<p>There are lots of good pubs in Cambridge.  Nearest to Selwyn, we recommend <a href="http://gkpubs.co.uk/pubs-in-cambridge/granta-pub/">The Granta</a>, and nearest to St John&rsquo;s, <a href="http://thecastleinncambridge.com/">The Castle</a>.  In the town centre, <a href="http://gkpubs.co.uk/pubs-in-cambridge/eagle-pub/">The Eagle</a> has particular historical interest.</p>
	<p>If you prefer something a bit more upmarket, try <a href="http://www.browns-restaurants.co.uk/locations/cambridge/">Browns restaurant</a>, or The Chop House has two branches &ndash; one on <a href="http://www.cambscuisine.com/cambridge-chop-house">King&rsquo;s Parade</a> and the other on <a href="http://www.cambscuisine.com/st-johns-chop-house/">Northampton Street</a> near St John&rsquo;s &ndash; and excellent food and wine.</p>
        <div class="embedded-gmap">
	    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d93fd35a57a1ed157&amp;ie=UTF8&amp;t=m&amp;ll=52.206029,0.118361&amp;spn=0.01841,0.036478&amp;z=14&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d93fd35a57a1ed157&amp;ie=UTF8&amp;t=m&amp;ll=52.206029,0.118361&amp;spn=0.01841,0.036478&amp;z=14&amp;source=embed">this map larger</a>.</small>
        </div>
                </section>

                <section id="staying">
                <h2>Staying in Cambridge</h2>
                <p>If you are thinking about staying overnight in Cambridge, either before or after the wedding, then this section may be helpful to you.  There are a wide variety of places to stay in Cambridge, with a correspondingly wide variety of prices, and a selection has been listed below.</p>
	<p>
At the cheaper end, the <a href="http://www.yha.org.uk/hostel/cambridge">YHA</a> and <a href="http://www.travelodge.co.uk/hotels/255/Cambridge-Central-hotel">Travelodge</a> are both near the station.  Guest rooms may be available in some of the colleges, and Selwyn College has a limited selection available specifically for wedding guests.  You can book rooms in other Cambridge colleges at reasonable prices using <a href="http://www.cambridgerooms.co.uk/">cambridgerooms.co.uk</a>.</p>
	<p>There are several large hotels, including the <a href="http://www.devere-hotels.co.uk/hotel-lodges/locations/university-arms.html">University Arms</a>, the <a href="http://www.forestdalehotels.com/Hotels/Central-and-Eastern-England/Royal-Cambridge-Hotel,-Cambridge">Royal Cambridge</a> and the <a href="http://cambridgecityhotel.co.uk/">Cambridge City Hotel</a>.</p>
        <div class="embedded-gmap">
	    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d94017c6bf9a288b6&amp;ie=UTF8&amp;t=m&amp;ll=52.202189,0.121279&amp;spn=0.018411,0.036478&amp;z=14&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d94017c6bf9a288b6&amp;ie=UTF8&amp;t=m&amp;ll=52.202189,0.121279&amp;spn=0.018411,0.036478&amp;z=14&amp;source=embed">this map larger</a>.</small>
        </div>
                </section>

                <section id="relaxing">
                <h2>Relaxing in Cambridge</h2>
                <p>During your stay in Cambridge, you may want to look around this beautiful city.  Below are some of the many attractions Cambridge has to offer.</p>
	<p>The University has many beautiful buildings. Take a look at <a href="http://www.kings.cam.ac.uk/">King&rsquo;s College</a> (Dan&rsquo;s college) and <a href="http://www.newn.cam.ac.uk/">Newnham College</a> (Rachel&rsquo;s college) for two contrasting styles.</p>
	<p>The <a href="http://www.fitzmuseum.cam.ac.uk/">Fitzwilliam Museum</a> is the University&rsquo;s arts and antiquities museum, for something more down-to-earth, try the <a href="http://www.folkmuseum.org.uk/">Cambridge and County Folk Museum</a>, or the <a href="http://www.cambridgesciencecentre.org/">Cambridge Science Centre</a> is always good fun.</p>
	<p>If the weather is fine, we suggest a walk by the river on Midsummer Common, or punts may be hired from <a href="http://www.scudamores.com/">Scudamores</a> on Magdelene Bridge or Mill Lane.  Or just take a seat and have tea and cake &ndash; there&rsquo;s plenty of places for this!</p>
        <div class="embedded-gmap">
	    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d9402cd6524f414bb&amp;ie=UTF8&amp;t=m&amp;ll=52.205766,0.115442&amp;spn=0.01841,0.036478&amp;z=14&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d9402cd6524f414bb&amp;ie=UTF8&amp;t=m&amp;ll=52.205766,0.115442&amp;spn=0.01841,0.036478&amp;z=14&amp;source=embed">this map larger</a>.</small>
        </div>
                </section>
                
	        </div>
            </div>
    </body>
</html>
