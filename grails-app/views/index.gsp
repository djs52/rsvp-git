<%@ page import="rsvp.Party" %>

<!doctype html>
<html>
    <head>
	<meta name="layout" content="bootstrap"/>
	<title>Dan and Rachel's wedding</title>
        <g:javascript>
            $('#RSVPcode').popover({
            placement: "left",
            html: true,
            title: "RSVP code",
            content: "Your RSVP code is written on the inside front page of your invitiation and looks like this:<br><i>123-456-789</i>"
            });
        </g:javascript>

    </head>

    <body>
        <div class="container">
	    <div class="row-fluid">

		<section id="main" class="span12 pagination-centered">

                    <div class="fp-block">
			<h1>Dan Sheridan & Rachel Chadwick</h1>
                        <hr class="wedding-rule-1"/>
			<h1>7th September 2013</h1>
                        <hr class="wedding-rule-2"/>
			<h1>Cambridge</h1>
		    </div>
		        <g:if test="${flash.message}">
			    <bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
		        </g:if>
                        <p>To RSVP and access the dinner menu selections, log in below using the code written on the inside front cover of your invitation.</p>
                        <p>You can come back to this site at any time before 31 July 2013 and change your choices.</p>
                        <g:form controller="party" action="login" class="form-inline form-rsvp">
                            <div class="input-append">
	                        <g:field class="input-large" type="text" name="RSVPcode" value="${fieldValue(bean: partyInstance, field: 'RSVPcode')}" placeholder="RSVP code"/>
                                <g:submitButton class="btn btn-wedding" name="submit" value="Go"/>
                            </div>
                        </g:form>
                        

		</section>
	    </div>
	</div>
    </body>
</html>
