<%@ page import="rsvp.MealChoice" %>
<%@ page import="rsvp.Course" %>
<!doctype html>
<html>
    <head>
	<meta name="layout" content="bootstrap"/>
	<title>Wedding breakfast menu</title>
    </head>
    
    <body data-spy="scroll" data-target="#sidenav" data-offset="50">
        
	    <div class="row-fluid">
		<div class="span3" id="sidenav">
		    <div class="well" data-spy="affix" data-offset-top="0">

                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#starters">Starters</a></li>
                            <li><a href="#mains">Mains</a></li>
                            <li><a href="#puddings">Puddings</a></li>
                            <li><a href="#children">Children's options</a></li>
    	                </ul>
                    </div>
                </div>

                <div class="span9">

		<div class="page-header">
                    <h1>Wedding breakfast menu</h1>
                </div>

                <p>
                    The full details of the menu are given below.
                </p>
                <p>
                    The caterers will make every effort to meet your
                    dietary requirements.  However, please be aware
                    that they will not be able to follow all rules of
                    kashrut regarding separation of foods or
                    slaughter. We have indicated which dishes are
                    dairy free, and no dishes mix meat and milk or
                    include meat from treif animals. The only
                    exception is the pancetta wrapped green beans
                    served with the duck.
                </p>

                <section id="starters">
                <h2>Starters</h2>
                <dl>
                    <g:each var="m" in="${MealChoice.findAllByCourse(Course.STARTER)}">
                        <dt>${m.name.encodeAsHTML()}</dt>
                        <dd>${m.description.encodeAsHTML()}</dd>
                    </g:each>
                </dl>
                </section>

                <section id="mains">
                <h2>Mains</h2>
                <dl>
                    <g:each var="m" in="${MealChoice.findAllByCourse(Course.MAIN)}">
                        <dt>${m.name.encodeAsHTML()}</dt>
                        <dd>${m.description.encodeAsHTML()}</dd>
                    </g:each>
                </dl>
                </section>

                <section id="puddings">
                <h2>Puddings</h2>
                <dl>
                    <g:each var="m" in="${MealChoice.findAllByCourse(Course.PUDDING)}">
                        <dt>${m.name.encodeAsHTML()}</dt>
                        <dd>${m.description.encodeAsHTML()}</dd>
                    </g:each>
                </dl>
                </section>

                <section id="children">
                <h2>Children's options</h2>
                Children will be served reduced size mains, followed by sorbet. Please see above for options.
                </section>



	        </div>
            </div>
    </body>
</html>
