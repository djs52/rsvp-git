<%@ page import="rsvp.MealChoice" %>



<div class="fieldcontain ${hasErrors(bean: mealChoiceInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="mealChoice.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="100" value="${mealChoiceInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: mealChoiceInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="mealChoice.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="255" value="${mealChoiceInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: mealChoiceInstance, field: 'course', 'error')} required">
	<label for="course">
		<g:message code="mealChoice.course.label" default="Course" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="course" from="${rsvp.Course?.values()}" keys="${rsvp.Course.values()*.name()}" required="" value="${mealChoiceInstance?.course?.name()}"/>
</div>

