<%@ page import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes" %>
<%@ page import="rsvp.Party" %>
<%@ page import="rsvp.InvitationType" %>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><g:layoutTitle default="${meta(name: 'app.name')}"/></title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="initial-scale = 1.0">

		<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<r:require modules="scaffolding"/>

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="72x72" href="${resource(dir: 'images', file: 'apple-touch-icon-72x72.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-114x114.png')}">

		<g:layoutHead/>
		<r:layoutResources/>
	</head>

	<body data-spy="${pageProperty(name:'body.data-spy')}" data-target="${pageProperty(name:'body.data-target')}" data-offset="${pageProperty(name:'body.data-offset')}">
            <div class="navbar-behind"></div>
            <div class="navbar-above"></div>

		<nav class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					
					<div class="nav-collapse">
					    <ul class="nav">
						<li<%= request.forwardURI == "${createLink(controller:'party', action:'rsvp')}" ? ' class="active"' : '' %>><a href="${createLink(controller:'party', action:'rsvp')}">RSVP</a></li>
						<li<%= request.forwardURI == "${createLink(uri: '/wedding')}" ? ' class="active"' : '' %>><a href="${createLink(uri: '/wedding')}">Wedding</a></li>
						<li<%= request.forwardURI == "${createLink(uri: '/cambridge')}" ? ' class="active"' : '' %>><a href="${createLink(uri: '/cambridge')}">Cambridge</a></li>
						<li<%= request.forwardURI == "${createLink(uri: '/gifts')}" ? ' class="active"' : '' %>><a href="${createLink(uri: '/gifts')}">Gifts</a></li>
                                                <g:if test="${session.user && (Party.get(session.user).type == InvitationType.FULL || Party.get(session.user).type == InvitationType.ADMIN)}">
						    <li<%= request.forwardURI == "${createLink(controller:'mealChoice', action:'menu')}" ? ' class="active"' : '' %>><a href="${createLink(controller:'mealChoice', action:'menu')}">Menu</a></li>
                                                </g:if>

                                                <g:if test="${session.user && Party.get(session.user).type == InvitationType.ADMIN}">
  
                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin<b class="caret"></b></a>
                                                        <ul class="dropdown-menu">
							    <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
							        <li<%= c.logicalPropertyName == controllerName ? ' class="active"' : '' %>><g:link controller="${c.logicalPropertyName}" action="list">${c.naturalName}</g:link></li>
							    </g:each>
                                                        </ul>
                                                    </li>
                                                </g:if>
					    </ul>
					</div>
				</div>
			</div>
		</nav>
                

		<div class="container-fluid">
			<g:layoutBody/>

                        <g:if test="${session.user}">
			    <footer class="text-center">
				<a href="http://www.danandrachel.org.uk">www.danandrachel.org.uk</a>
                                <g:img file="purple-icon.png"/>
				<a href="mailto:rsvp@danandrachel.org.uk">rsvp@danandrachel.org.uk</a>
                                <g:img file="orange-icon.png"/>
                                07977 571782
                                <g:img file="red-icon.png"/>
                                21a Wolsey Road, London N1 4QH
			    </footer>
                        </g:if>
		</div>

		<r:layoutResources/>

	</body>
</html>
