
<%@ page import="rsvp.Guest" %>
<%@ page import="rsvp.RSVPStatus" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'guest.label', default: 'Guest')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
                                <g:form action="list">
                                    <select name="filter" onchange="submit()" value="${filter}">
                                        <option>(filter by response)</option>
                                        <option ${filter=="NO_RESPONSE" ? 'selected=selected' : ''}>${RSVPStatus.NO_RESPONSE}</option>
                                        <option ${filter=="COMING" ? 'selected=selected' : ''}>${RSVPStatus.COMING}</option>
                                        <option ${filter=="NOT_COMING" ? 'selected=selected' : ''}>${RSVPStatus.NOT_COMING}</option>
                                    </select>
                                    ${guestInstanceTotal} Guest${guestInstanceTotal==1 ? '' : 's'}
                                </g:form>

				<table class="table table-striped">
					<thead>
						<tr>
						
							<g:sortableColumn property="name" title="${message(code: 'guest.name.label', default: 'Name')}" params="['filter':filter]"/>
						
							<g:sortableColumn property="response" title="${message(code: 'guest.response.label', default: 'Response')}" params="['filter':filter]"/>
						
							<th class="header"><g:message code="guest.starter.label" default="Starter" /></th>
						
							<th class="header"><g:message code="guest.main.label" default="Main" /></th>
						
							<th class="header"><g:message code="guest.pudding.label" default="Pudding" /></th>
						
							<g:sortableColumn property="dietaryRequirements" title="${message(code: 'guest.dietaryRequirements.label', default: 'Dietary Requirements')}" params="['filter':filter]"/>
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${guestInstanceList}" var="guestInstance">
						<tr>
						
							<td>${fieldValue(bean: guestInstance, field: "name")}</td>
						
							<td>${fieldValue(bean: guestInstance, field: "response")}</td>
						
							<td>${fieldValue(bean: guestInstance, field: "starter")}</td>
						
							<td>${fieldValue(bean: guestInstance, field: "main")}</td>
						
							<td>${fieldValue(bean: guestInstance, field: "pudding")}</td>
						
							<td>${fieldValue(bean: guestInstance, field: "dietaryRequirements")}</td>
						
							<td class="link">
								<g:link action="show" id="${guestInstance.id}" class="btn btn-small">Show &raquo;</g:link>
							</td>
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${guestInstanceTotal}" params="['filter':filter]"/>
				</div>
			</div>

		</div>
	</body>
</html>
