
<%@ page import="rsvp.Guest" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'guest.label', default: 'Guest')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>
				
					<g:if test="${guestInstance?.name}">
						<dt><g:message code="guest.name.label" default="Name" /></dt>
						
							<dd><g:fieldValue bean="${guestInstance}" field="name"/></dd>
						
					</g:if>
				
					<g:if test="${guestInstance?.child}">
						<dt><g:message code="child.name.label" default="Child" /></dt>
						
							<dd><g:fieldValue bean="${guestInstance}" field="child"/></dd>
						
					</g:if>
				
					<g:if test="${guestInstance?.response}">
						<dt><g:message code="guest.response.label" default="Response" /></dt>
						
							<dd><g:fieldValue bean="${guestInstance}" field="response"/></dd>
						
					</g:if>
				
					<g:if test="${guestInstance?.starter}">
						<dt><g:message code="guest.starter.label" default="Starter" /></dt>
						
							<dd><g:link controller="mealChoice" action="show" id="${guestInstance?.starter?.id}">${guestInstance?.starter?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${guestInstance?.main}">
						<dt><g:message code="guest.main.label" default="Main" /></dt>
						
							<dd><g:link controller="mealChoice" action="show" id="${guestInstance?.main?.id}">${guestInstance?.main?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${guestInstance?.pudding}">
						<dt><g:message code="guest.pudding.label" default="Pudding" /></dt>
						
							<dd><g:link controller="mealChoice" action="show" id="${guestInstance?.pudding?.id}">${guestInstance?.pudding?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${guestInstance?.dietaryRequirements}">
						<dt><g:message code="guest.dietaryRequirements.label" default="Dietary Requirements" /></dt>
						
							<dd><g:fieldValue bean="${guestInstance}" field="dietaryRequirements"/></dd>
						
					</g:if>
				
					<g:if test="${guestInstance?.party}">
						<dt><g:message code="guest.party.label" default="Party" /></dt>
						
							<dd><g:link controller="party" action="show" id="${guestInstance?.party?.id}">${guestInstance?.party?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${guestInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn" action="edit" id="${guestInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
