<%@ page import="rsvp.Guest" %>
<%@ page import="rsvp.Course" %>
<%@ page import="rsvp.RSVPStatus" %>
<%@ page import="rsvp.InvitationType" %>

<section id="id${guestInstance.id}">
    <form class="form-horizontal">
        <g:if test="${guestInstance.name}">
            <legend>${guestInstance.name.encodeAsHTML()}</legend>
        </g:if><g:else>
            <legend>Guest</legend>
            
        <div class="control-group guestname">
            <div class="controls">
	        <g:field class="input-large guestname" type="text" name="name_${guestInstance.id}" placeholder="Guest name"/>                                    
                <i id="result_name_${guestInstance.id}"></i><span id="comment_name_${guestInstance.id}"></span>

            </div>
        </div>

        </g:else>

        <div class="control-group rsvp">
            <div class="controls">
                <div class="btn-group" data-toggle="buttons-radio" accordion="#accordion_${guestInstance.id}">
                    <button id="rsvpyes_${guestInstance.id}"
                            type="button" 
                            class="btn ${guestInstance.response==RSVPStatus.COMING ? 'active' : ''}">
                        <i class="icon-thumbs-up"></i> Yes, I&rsquo;m coming
                    </button>
                    <button id="rsvpno_${guestInstance.id}"
                            type="button" 
                            class="btn ${guestInstance.response==RSVPStatus.NOT_COMING ? 'active' : ''}">
                        <i class="icon-thumbs-down"></i> No, I can&rsquo;t come
                    </button>
                </div>
                <i id="result_rsvp_${guestInstance.id}"></i> <span id="comment_rsvp_${guestInstance.id}"></span>
            </div>
        </div>

        <%-- only show food information for full guests --%>
        <g:if test="${guestInstance.party.type == InvitationType.FULL}">

            <div id="accordion_${guestInstance.id}" class="collapse ${guestInstance.response==RSVPStatus.COMING ? 'in' : ''}">

                <p>Please choose the food you&rsquo;d like to have at the wedding breakfast.</p>

                <%-- children's menu, with a "no food" option --%>
                
                <g:if test="${guestInstance.child}">
                    <div class="control-group menu">
                        <label class="control-label" for="main_${guestInstance.id}">Child's main</label>
                        <div class="controls">
                            <div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
                                <button id="rsvp_main_0_${guestInstance.id}"
                                        type="button" 
                                        class="btn btn-food btn-block ${guestInstance?.main == null ? 'active' : ''}"
                                        title="No food will be provided for this guest">
                                    No food
                                </button>
                                <g:each var="m" in="${rsvp.MealChoice.findAllByCourse(Course.MAIN)}">
                                    <button id="rsvp_main_${m.id}_${guestInstance.id}"
                                            type="button" 
                                            class="btn btn-food btn-block ${guestInstance?.main?.id==m.id ? 'active' : ''}"
                                            title="${m.description.encodeAsHTML()}">
                                        ${m.encodeAsHTML()}
                                    </button>
                                </g:each>
                            </div>
                            <i id="result_main_${guestInstance.id}"></i> <span id="comment_main_${guestInstance.id}"></span>
                        </div>
                    </div>
                </g:if>
                <g:else>

                    <div class="control-group menu">
                        <label class="control-label" for="starter_${guestInstance.id}">Starter</label>
                        <div class="controls">
                            <div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
                                <g:each var="m" in="${rsvp.MealChoice.findAllByCourse(Course.STARTER)}">
                                    <button id="rsvp_starter_${m.id}_${guestInstance.id}"
                                            type="button" 
                                            class="btn btn-food btn-block ${guestInstance?.starter?.id==m.id ? 'active' : ''}"
                                            title="${m.description.encodeAsHTML()}">
                                        ${m.encodeAsHTML()}
                                    </button>
                                </g:each>
                            </div>
                            <i id="result_starter_${guestInstance.id}"></i> <span id="comment_starter_${guestInstance.id}"></span>
                        </div>
                    </div>
                    
                    <div class="control-group menu">
                        <label class="control-label" for="main_${guestInstance.id}">Main</label>
                        <div class="controls">
                            <div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
                                <g:each var="m" in="${rsvp.MealChoice.findAllByCourse(Course.MAIN)}">
                                    <button id="rsvp_main_${m.id}_${guestInstance.id}"
                                            type="button" 
                                            class="btn btn-food btn-block ${guestInstance?.main?.id==m.id ? 'active' : ''}"
                                            title="${m.description.encodeAsHTML()}">
                                        ${m.encodeAsHTML()}
                                    </button>
                                </g:each>
                            </div>
                            <i id="result_main_${guestInstance.id}"></i> <span id="comment_main_${guestInstance.id}"></span>
                        </div>
                    </div>
                    <div class="control-group menu">
                        <label class="control-label" for="pudding_${guestInstance.id}">Pudding</label>
                        <div class="controls">
                            <div class="btn-group btn-group-vertical" data-toggle="buttons-radio">
                                <g:each var="m" in="${rsvp.MealChoice.findAllByCourse(Course.PUDDING)}">
                                    <button id="rsvp_pudding_${m.id}_${guestInstance.id}"
                                            type="button" 
                                            class="btn btn-food btn-block ${guestInstance?.pudding?.id==m.id ? 'active' : ''}"
                                            title="${m.description.encodeAsHTML()}">
                                        ${m.encodeAsHTML()}
                                    </button>
                                </g:each>
                            </div>
                            <i id="result_pudding_${guestInstance.id}"></i> <span id="comment_pudding_${guestInstance.id}"></span>
                        </div>
                    </div>
                </g:else>

                <div class="control-group">
                    <div class="controls">
                        <a href="#dietary_${guestInstance.id}" role="button" class="btn btn-block btn-group-vertical btn-food" data-toggle="modal">Dietary requirements</a>
                        <i id="result_dietary_${guestInstance.id}"></i> <span id="comment_dietary_${guestInstance.id}"></span>
                    </div>

                </div>
            </div>
        </g:if>

    </form>

    <%-- Modal --%>
    <div id="dietary_${guestInstance.id}" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="dietaryLabel_${guestInstance.id}" aria-hidden="true">
        <%-- grails is a bit too helpful -- we can't use the
        generating functions in ajax-rsvp.js in advance, so call
        them and then call their results --%>
	<g:formRemote name="dietary_${guestInstance.id}" 
                      url="[controller:'guest', action:'ajaxDiet', params:[id:guestInstance?.id]]" 
                      class="form-horizontal" 
                      action="edit" 
                      id="${guestInstance?.id}" 
                      after="\$('#dietary_${guestInstance.id}').modal('hide')"
                      onSuccess="successGen('dietary_'+${guestInstance.id})(data,textStatus)"
                      onFailure="errorGen('dietary_'+${guestInstance.id})(XMLHttpRequest,textStatus,errorThrown)">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="dietaryLabel_${guestInstance.id}">Dietary requirements<br/>
                <small>${guestInstance.name.encodeAsHTML()}</small></h3>
            </div>
            <div class="modal-body">
	        <g:textArea name="dietaryRequirements" class="modal-input" rows="5" maxlength="255" value="${guestInstance?.dietaryRequirements}"/>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button class="btn btn-primary">Save changes</button>
            </div>
        </g:formRemote>
    </div>



</section>
