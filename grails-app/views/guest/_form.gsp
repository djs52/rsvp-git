<%@ page import="rsvp.Guest" %>



<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="guest.name.label" default="Name" />
		
	</label>
	<g:textArea name="name" cols="40" rows="5" maxlength="255" value="${guestInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'child', 'error')} required">
	<label for="child">
		<g:message code="guest.child.label" default="Child" />
		<span class="required-indicator">*</span>
	</label>
	<g:checkBox name="child" value="${guestInstance?.child}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'response', 'error')} required">
	<label for="response">
		<g:message code="guest.response.label" default="Response" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="response" from="${rsvp.RSVPStatus?.values()}" keys="${rsvp.RSVPStatus.values()*.name()}" required="" value="${guestInstance?.response?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'starter', 'error')} ">
	<label for="starter">
		<g:message code="guest.starter.label" default="Starter" />
		
	</label>
	<g:select id="starter" name="starter.id" from="${rsvp.MealChoice.list()}" optionKey="id" value="${guestInstance?.starter?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'main', 'error')} ">
	<label for="main">
		<g:message code="guest.main.label" default="Main" />
		
	</label>
	<g:select id="main" name="main.id" from="${rsvp.MealChoice.list()}" optionKey="id" value="${guestInstance?.main?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'pudding', 'error')} ">
	<label for="pudding">
		<g:message code="guest.pudding.label" default="Pudding" />
		
	</label>
	<g:select id="pudding" name="pudding.id" from="${rsvp.MealChoice.list()}" optionKey="id" value="${guestInstance?.pudding?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'dietaryRequirements', 'error')} ">
	<label for="dietaryRequirements">
		<g:message code="guest.dietaryRequirements.label" default="Dietary Requirements" />
		
	</label>
	<g:textArea name="dietaryRequirements" cols="40" rows="5" maxlength="255" value="${guestInstance?.dietaryRequirements}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: guestInstance, field: 'party', 'error')} required">
	<label for="party">
		<g:message code="guest.party.label" default="Party" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="party" name="party.id" from="${rsvp.Party.list()}" optionKey="id" required="" value="${guestInstance?.party?.id}" class="many-to-one"/>
</div>

