<!doctype html>
<html>
    <head>
	<meta name="layout" content="bootstrap"/>
	<title>Wedding information</title>
    </head>
    
    <body data-spy="scroll" data-target="#sidenav" data-offset="50">
        <!-- <header class="jumbotron subhead" id="overview"> -->
        <!--     <div class="container"> -->
        <!--         <h1>Wedding information</h1> -->
        <!--         <p class="lead">Places to stay, eat, park; things to buy, things to wear</p> -->
        <!--     </div> -->
        <!-- </header> -->
        
	    <div class="row-fluid">
		<div class="span3" id="sidenav">
		    <div class="well" data-spy="affix" data-offset-top="0">

                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#timings">Timings</a></li>
                            <li><a href="#venues">Venues</a></li>
                            <li><a href="#transport">Transport</a></li>
                            <li><a href="#costume">Costume</a></li>
    	            <li><a href="#confetti">Confetti</a></li>
                        </ul>
                    </div>
                </div>

                <div class="span9">

                <section class="first">
                <h2>Timings</h2>
                <p>The ceremony will begin at 1pm.  We would appreciate it if you could be seated promptly 10 minutes beforehand.  After the ceremony there will be drinks and photographs in the gardens.  Food will not be served at this point, so we encourage all guests to eat a light lunch beforehand.</p>
	<p>For those guests who are atttending the wedding breakfast, this will run from roughly 4pm until 7pm.</p>
	<p>All guests are then welcome to the evening ceilidh/contra dance, which will begin at 7.30pm and continue until 11pm.</p>
                </section>

                <section id="venues">
                <h2>Venues</h2>
                <p>The ceremony itself will be held in Selwyn College Chapel.  The Chapel is reached by entering through the main College entrance on Grange Road.  After the ceremony, there will be drinks and photographs in the adjoining gardens.</p>
	<p>The second part of the day will take place in a marquee on the St John&rsquo;s College playing fields.  This can be reached by a short footpath or track from Grange Road.</p>
        <div class="embedded-gmap">
	    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d940c2140a99e0348&amp;ie=UTF8&amp;t=m&amp;ll=52.205187,0.107975&amp;spn=0.03682,0.072956&amp;z=13&amp;output=embed"></iframe><br /><small>View <a href="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202440398723805798752.0004d940c2140a99e0348&amp;ie=UTF8&amp;t=m&amp;ll=52.205187,0.107975&amp;spn=0.03682,0.072956&amp;z=13&amp;source=embed">this map larger</a>.</small>
        </div>
                </section>

                <section id="transport">
                <h2>Transport</h2>
                <p>As there is limited car parking at both the venues, we encourage you to park elsewhere or use alternative forms of transport.  If you require a parking space close to the venue for some reason (e.g. if you are infirm, disabled, or have small children) please let us know and we will try to accommodate you.  The marquee on St John&rsquo;s playing field is about 15 minutes walk from Selwyn College.</p>
	<p>Cambridge is easily accessible by train; however the station is about 40 minutes walk from the Chapel, so please leave plenty of time.  Alternatively, there are three bus routes that run from the station to the city centre &ndash; downloadable timetables are available here for the <a href="http://www.stagecoachbus.com/PdfUploads/Timetable_27261_%20Cambridge%20citi%201.pdf">Citi 1</a>, <a href="http://www.stagecoachbus.com/getTimetable.ashx?code=XECO003&dir=INBOUND&date=01%2f04%2f2013">Citi 3</a> and <a href="http://www.stagecoachbus.com/getTimetable.ashx?code=XECO007&dir=OUTBOUND&date=01%2f04%2f2013">Citi 7</a>.  The Uni 4 runs between Grange Road and Hills Road, a short walk from the station (timetable <a href="http://www.stagecoachbus.com/getTimetable.ashx?code=XEAUNI4&dir=INBOUND&date=01%2f04%2f2013">here</a>).  For taxis, try <a href="http://www.panthertaxis.co.uk/">Panther</a> or <a href="http://www.a1cabco.co.uk/">A1</a>, and there are taxi ranks on St Andrew&rsquo;s Street and outside the station.</p>
	<p>If you are driving into Cambridge, take a look at the <a href="https://www.cambridge.gov.uk/car-parks-map">Cambridge County Council website</a> for car park locations or try your luck parking on the street in Newnham Village or on Queen&rsquo;s Road.</p>
<p>Please note that the <a href="http://traintimes.org.uk/cambridge/london/last/saturday">last train back to London</a> leaves Cambridge station at 11.15.</p>
                </section>
                
                <section>
                <h2 id="costume">Costume</h2>
                <p>Please wear smart clothing appropriate to a wedding, but please also be prepared for dancing in the evening.</p>
                </section>

	<section>
                <h2 id="confetti">Confetti</h2>
                <p>Feel free to bring biodegradable confetti with you.</p>
                </section>


	        </div>
            </div>
    </body>
</html>
