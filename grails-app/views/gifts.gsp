<!doctype html>
<html>
    <head>
	<meta name="layout" content="bootstrap"/>
	<title>Gift list and helping hands</title>
    </head>
    
    <body data-spy="scroll" data-target="#sidenav" data-offset="50">
        
	    <div class="row-fluid">
		<div class="span3" id="sidenav">
		    <div class="well" data-spy="affix" data-offset-top="0">

                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#gifts">Gift List</a></li>
                            <li><a href="#helping">Helping Hands</a></li>
    	                </ul>
                    </div>
                </div>

                <div class="span9">

                <section class="first">
                <h2>Gift List</h2>
                <p>If you want to purchase a gift to help us start our new life together, you can find our gift lists at <a href="http://www.johnlewisgiftlist.com/giftint/JSPs/GiftList/BuyGifts/GuestFindAList.jsp" target="_blank">John Lewis</a>, list number 545655, and at <a href="http://www.amazon.co.uk/registry/wedding/WA1FARDVX4Z" target="_blank">Amazon</a>. The lists open on 27th July.  Thank you!</p>
		<p> Financial contributions towards our honeymoon are also very welcome - please contact <a href="mailto:chris.chadwick@which.net">chris.chadwick@which.net</a> for details.  If you would prefer to make a charitable donation instead of a gift, our preferred charity is <a href="http://www.meresearch.org.uk">ME Research UK</a>.</p>
                </section>

                <section id="helping">
                <h2>Helping Hands</h2>
                <p>We would very much appreciate some help on the day.  If you think that you could give us a hand (however big or small), please let us know.</p>
                </section>

	        </div>
            </div>
    </body>
</html>
