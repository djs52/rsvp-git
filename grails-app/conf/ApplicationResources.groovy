modules = {
    application {
        resource url:'js/application.js'
    }
    rsvp {
        resource url:'js/ajax-rsvp.js'
    }
}