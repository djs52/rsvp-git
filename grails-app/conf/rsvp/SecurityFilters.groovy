package rsvp;
import rsvp.*;

class SecurityFilters {

    def filters = {
        loginCheck(controller: '*', action: '*') {
            before = {

                if (!controllerName) return true; // login page and static pages

                if (!session.user) {
                    if (actionName == "login") {
                        return true;
                    } else {
                        // don't redirect ajax actions
                        if (actionName.startsWith("ajax")) {
                            render(status: 401, text: "failed, please log in again");
                            return false;
                        } else {
                            redirect(uri:'/');
                            return false;
                        }
                    }
                    
                } else {

                    def type = Party.get(session.user).type;

                    // protect default CRUD actions
                    if (actionName in ['create', 'edit', 'list', 'delete', 'show']) {
                        if (type == InvitationType.ADMIN) {
                            return true;
                        } else {
                            redirect(controller:'party', action:'rsvp');
                        }
                    } else {
                        // for other actions, detailed access control is in the controller
                        return true;
                    }
                }
            }
        }
    }
}
