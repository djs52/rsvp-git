class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(view:"/index")
		"/cambridge"(view:"/cambridge")
		"/wedding"(view:"/wedding")
		"/gifts"(view:"/gifts")
		"500"(view:'/error')
	}
}
