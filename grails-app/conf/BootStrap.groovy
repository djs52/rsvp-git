import rsvp.*

class BootStrap {

    def fixtureLoader

    def init = { 
      servletContext ->
      
      if (Guest.count() == 0 && MealChoice.count() == 0) {
          fixtureLoader.load("realdata")
      }
                 

    }
    def destroy = {
    }
}
