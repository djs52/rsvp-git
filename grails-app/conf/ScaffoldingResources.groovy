modules = {
	scaffolding {
                dependsOn 'jquery'
                resource url: 'css/bootstrap.css'
                resource url: 'css/wedding.css'
                resource url: 'js/bootstrap.js'
                resource url: 'css/bootstrap-fixtaglib.css'
		resource url: 'css/scaffolding.css'
                resource url: 'css/fonts.css'
	}
}