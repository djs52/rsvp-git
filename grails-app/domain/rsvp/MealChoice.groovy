package rsvp

class MealChoice {

    Course course;
    String name;
    String description;

    static hasMany = [guests_starter:Guest,guests_main:Guest,guests_pudding:Guest];
    static mappedBy = [guests_starter:'starter', guests_main:'main', guests_pudding:'pudding'];

    static transients = ['guests'];

    static mapping = {
        sort "id"
    }

    static constraints = {
        name(size:0..100);
        name(size:0..255);
        course();
    }

    String toString() {
        name;
    }

    Guest[] getGuests() {
        switch (course) {
        case Course.STARTER: return guests_starter;
        case Course.MAIN: return guests_main;
        case Course.PUDDING: return guests_pudding;
        }
    }

}
