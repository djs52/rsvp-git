package rsvp

class Party {

    String name; // should be unique, so the hash is stable
    String displayName; // should be human-oriented, defaults to name
    String emergencyPhone = "";
    String emergencyEmail = "";
    String notes;
    Integer RSVPcode;
    InvitationType type = InvitationType.FULL;
    
    static hasMany = [guests:Guest];

    static constraints = {
        name(size:0..100);
        displayName(size:0..100, nullable: true);
        emergencyPhone(size:0..30);
        emergencyEmail(email:true, size:0..100);
        notes(nullable:true, size:0..255);
        RSVPcode(range:100000000..999999999, unique: true); // 9-digit code with no leading zeroes
        type();
    }
        
    String toString() {
        "Party: ${displayName} (${guests?.join(", ") ?: 'no guests'})"
    }

    String getDisplayName() {
        displayName ?: name;
    }

    def beforeValidate() {
        if (name && RSVPcode == null) {
            RSVPcode = Math.abs(name.hashCode() % 899999999) + 100000000
            log.info "New RSVPcode for ${name}: ${RSVPcode}"
        }
    }

}
