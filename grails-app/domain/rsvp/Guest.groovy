package rsvp

class Guest {

    String name;
    RSVPStatus response = RSVPStatus.NO_RESPONSE;
    MealChoice starter;
    MealChoice main;
    MealChoice pudding;
    String dietaryRequirements;
    Boolean child = false;

    Party party;

    static mapping = {
        sort "id"
    }

    static constraints = {
        name(size:0..255, nullable:true); // +1 guests have null names
        response();
        starter(nullable:true);
        main(nullable:true);
        pudding(nullable:true);
        dietaryRequirements(nullable:true, size:0..255);
    }

    String toString() {
        "Guest: ${name?:'+1'}";
    }

}
